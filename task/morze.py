def code_morze(value):
    dictionary = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....',
                   'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.',
                   'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-',
                   'Y': '-.--', 'Z': '--..', '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....',
                   '6': '-....', '7': '--...', '8': '---..', '9': '----.', '0': '-----', ', ': '--..--', '.': '.-.-.-',
                   '?': '..--..', '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'}
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    new_string = ""
    if not isinstance(value, str):
        return False
    # for index in range(len(value)):
    #     if value[index] in dictionary.keys():
    #         value = value.replace(letter, dictionary[letter])
    for letter in value:
        new_string += (dictionary[letter.upper()] + ' ') if letter.upper() in dictionary else ''

    # new_string = new_string.join((dictionary[letter.upper()] if letter.upper() in dictionary else ' ')
    #                              for letter in value)
    # new_string = new_string.join((dictionary[letter.upper()] if letter.upper() in dictionary else ' ')
    #                              for letter in value)

    return new_string.rstrip()
